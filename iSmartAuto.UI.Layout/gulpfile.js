const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const cssnano = require('gulp-cssnano');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');
const del = require('del');


// Compile scss into CSS & auto-inject into browsers
gulp.task('compile-scss', function() {
    return gulp.src('src/scss/*.scss')
        .pipe(sass().on('error',sass.logError))
        .pipe(gulp.dest("src/css"))
        .pipe(browserSync.stream());
});

// Minification css and add prefix 'min'
gulp.task('min-css', function(){
    return gulp.src(['src/css/*.css','!src/css/**/*.min.css'])
            .pipe(cssnano())
            .pipe(rename({suffix: '.min'}))
            .pipe(gulp.dest('src/css'));
});

// Minification js and add prefix 'min'
gulp.task('min-js', function(){
    return gulp.src(['src/js/*.js', '!src/js/**/*.min.js'])
            .pipe(uglify())
            .pipe(rename({suffix: '.min'}))
            .pipe(gulp.dest('src/js'));
})

// Static Server + watching scss/html files
gulp.task('watch', function() {
    browserSync.init({
        server: "./src"
    });
    gulp.watch('src/scss/**/*.scss', gulp.series('compile-scss', 'min-css'));
    gulp.watch("src/*.html").on("change", browserSync.reload);
    gulp.watch("src/js/*.js").on("change", gulp.series('min-js'));
    gulp.watch("src/js/*.js").on("change", browserSync.reload);
});

gulp.task('default', gulp.series('compile-scss', 'min-css', 'min-js', 'watch'));

gulp.task('clear-files', function(){
    return del.sync('dist');
})

gulp.task('build-dist', function() { 
    gulp.series('clear-files', 'compile-scss', 'min-css', 'min-js');

    // Build css
    gulp.src('src/css/*.min.css').pipe(gulp.dest('dist/css'));
    // Build html               
    gulp.src('src/**/*.html').pipe(gulp.dest('dist'));
    // Build js
    gulp.src('src/js/**/*.min.js').pipe(gulp.dest('dist/js'));
    // Build libs          
    gulp.src('src/libs/**/*.*').pipe(gulp.dest('dist/libs/'));

    // Copy assets
    return gulp.src('src/assets/**/*').pipe(gulp.dest('dist/assets'));
 });

 gulp.task('build-prod', function() { 
    gulp.series('compile-scss', 'min-css', 'min-js');

    var destinationPath = "../iSmartAuto/iSmartAuto.UI/wwwroot/"; 

    // Build css
    gulp.src('src/css/*.min.css').pipe(gulp.dest(destinationPath + '/css'));
    // Build html               
    gulp.src('src/**/*.html').pipe(gulp.dest(destinationPath + ''));
    // Build js
    gulp.src('src/js/**/*.min.js').pipe(gulp.dest(destinationPath + '/js'));
    // Build libs          
    gulp.src('src/libs/**/*.*').pipe(gulp.dest(destinationPath + '/libs'));

    // Copy assets
    return gulp.src('src/assets/**/*').pipe(gulp.dest(destinationPath + '/assets'));
 });


// Move the javascript files into our /src/js folder
// gulp.task('move-js', function() {
//     return gulp.src(['node_modules/bootstrap/dist/js/bootstrap.min.js', 'node_modules/jquery/dist/jquery.min.js', 'node_modules/popper.js/dist/umd/popper.min.js'])
//         .pipe(gulp.dest("src/js"))
//         .pipe(browserSync.stream());
// });