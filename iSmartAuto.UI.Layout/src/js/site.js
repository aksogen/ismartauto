$(document).ready(function() 
{
    $(document).on('click', '.sort-item', function (e) {
        $('.sort-item').removeClass('active');
        $(e.currentTarget).addClass('active');
    });

    $(document).on('click', '.page-item', function (e) {
        e.preventDefault();
        // window.history.pushState("object or string", "Title", "/new-url");

        $('.page-item').removeClass('active');
        $(e.currentTarget).addClass('active');
    });

});