﻿using System.Threading.Tasks;
using Microsoft.ML;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using iSmartAuto.Domain.Interfaces.Services.CarOffers.Prediction;
using iSmartAuto.Domain.Services.CarOffers.Prediction;
using iSmartAuto.Domain.Interfaces.Services.ThirdParty;
using iSmartAuto.Domain.Services.ThirdParty;
using Microsoft.Extensions.Logging;
using iSmartAuto.Domain.Services.DataCollection;
using Microsoft.Extensions.Configuration;
using System.IO;
using iSmartAuto.Infrastructure.Configs.ThirdParty;
using System;
using iSmartAuto.Domain.Entities.CarOffer;
using iSmartAuto.Domain.Interfaces.Services.CarOffers;
using iSmartAuto.Domain.Services.CarOffers;
using System.Net.Http;
using iSmartAuto.Infrastructure.Configs.ML;
using System.Linq;

namespace iSmartAuto.Console
{
    public class Program
    {
        static async Task Main(string[] args)
        {
            // ConfigureBuilder
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            IConfigurationRoot configuration = builder.Build();

            // Setup our DI
            var serviceProvider = new ServiceCollection()
                .AddSingleton<MLContext>()
                .AddSingleton<IAutoruService, AutoruService>()
                .AddSingleton<DataCollectorService>()
                .AddSingleton<IPredictionModelService, PredictionModelService>()
                .Configure<AutoruConfig>(configuration.GetSection(nameof(AutoruConfig)))
                .Configure<MLCarOffersConfig>(configuration.GetSection(nameof(MLCarOffersConfig)))
                .AddLogging(loggingBuilder =>
                {
                    loggingBuilder.AddConsole(options =>
                    {
                        options.IncludeScopes = true;
                    });
                    loggingBuilder.AddDebug();
                })
                .BuildServiceProvider();

            var dataCollectorService = serviceProvider.GetService<DataCollectorService>();
            var predictionModelService = serviceProvider.GetService<IPredictionModelService>();

            //await dataCollectorService.CollectJsonAsync();
            //dataCollectorService.ParseJsonToCsv();

            //predictionModelService.CreatePredictionModels();
        }
    }
}