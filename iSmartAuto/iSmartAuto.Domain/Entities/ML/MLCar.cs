﻿using Microsoft.ML.Data;
using System;

namespace iSmartAuto.Domain.Entities.ML
{
    public class MLCar
    {
        [LoadColumn(1000)]
        public string Id { get; set; } 

        [LoadColumn(0)]
        public string Brand { get; set; }

        [LoadColumn(1)]
        public string Model { get; set; }

        [LoadColumn(2)]
        public string Generation { get; set; }

        [LoadColumn(3)]
        public string BodyType { get; set; }
        
        [LoadColumn(4)]
        public string EngineType { get; set; }
        
        [LoadColumn(5)]
        public string TransmissionType { get; set; }
        
        [LoadColumn(6)]
        public string GearType { get; set; }
        
        [LoadColumn(7)]
        public string PriceSegment { get; set; }
        
        [LoadColumn(8)]
        public float Power { get; set; }
        
        [LoadColumn(9)]
        public float FuelRate { get; set; }
        
        [LoadColumn(10)]
        public float Price { get; set; }
        
        [LoadColumn(11)]
        public float Owners { get; set; }
        
        [LoadColumn(12)]
        public float Year { get; set; }
        
        [LoadColumn(13)]
        public float Mileage { get; set; }
    }
}
