﻿using Microsoft.ML.Data;
using System;

namespace iSmartAuto.Domain.Entities.ML
{
    public class MLCarPricePrediction
    {
        public string Id { get; set; }

        [ColumnName("Score")]
        public float Price;
    }
}
