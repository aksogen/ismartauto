﻿using Newtonsoft.Json;

namespace iSmartAuto.Domain.Entities.ThirdParty.Autoru
{
    public class Configuration
    {
        [JsonProperty("body_type")]
        public string BodyType { get; set; }

        [JsonProperty("auto_class")]
        public string AutoClass { get; set; }

        [JsonProperty("body_type_group")]
        public string BodyTypeGroup { get; set; }
    }
}