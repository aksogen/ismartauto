﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace iSmartAuto.Domain.Entities.ThirdParty.Autoru
{
    public class State
    {
        [JsonProperty("mileage")]
        public long Mileage { get; set; }
        [JsonProperty("image_urls")]
        public List<ImageUrl> ImageUrls { get; set; }

        [JsonProperty("state_not_beaten")]
        public bool StateNotBeaten { get; set; }
    }
}