﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace iSmartAuto.Domain.Entities.ThirdParty.Autoru
{
    public class AutoruResponse
    {
        [JsonProperty("offers")]
        public List<CarOffer> CarOffers { get; set; }

        [JsonProperty("pagination")]
        public Pagination Pagination { get; set; }
    }
}
