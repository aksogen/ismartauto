﻿using Newtonsoft.Json;

namespace iSmartAuto.Domain.Entities.ThirdParty.Autoru
{
    public class Location
    {
        [JsonProperty("region_info")]
        public RegionInfo RegionInfo { get; set; }
    }
}