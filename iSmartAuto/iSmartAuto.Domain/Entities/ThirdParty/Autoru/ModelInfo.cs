﻿using Newtonsoft.Json;

namespace iSmartAuto.Domain.Entities.ThirdParty.Autoru
{
    public class ModelInfo
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}