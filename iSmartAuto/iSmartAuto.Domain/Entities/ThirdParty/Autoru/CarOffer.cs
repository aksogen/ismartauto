﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace iSmartAuto.Domain.Entities.ThirdParty.Autoru
{
    public class CarOffer
    {
        [JsonProperty("car_info")]
        public CarInfo CarInfo { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("color_hex")]
        public string ColorHex { get; set; }

        [JsonProperty("section")]
        public string Section { get; set; }

        [JsonProperty("price_info")]
        public PriceInfo PriceInfo { get; set; }

        [JsonProperty("documents")]
        public Documents Documents { get; set; }

        [JsonProperty("state")]
        public State State { get; set; }

        [JsonProperty("seller_type")]
        public string SellerType { get; set; }

        [JsonProperty("seller")]
        public Seller Seller { get; set; }
    }
}
