﻿using Newtonsoft.Json;

namespace iSmartAuto.Domain.Entities.ThirdParty.Autoru
{
    public class CarInfo
    {
        [JsonProperty("mark")]
        public string Mark { get; set; }

        [JsonProperty("model")]
        public string Model { get; set; }

        [JsonProperty("body_type")]
        public string BodyType { get; set; }

        [JsonProperty("engine_type")]
        public string EngineType { get; set; }

        [JsonProperty("transmission")]
        public string Transmission { get; set; }

        [JsonProperty("drive")]
        public string Drive { get; set; }

        [JsonProperty("wheel_left")]
        public bool WheelLeft { get; set; }

        [JsonProperty("horse_power")]
        public double HorsePower { get; set; }

        [JsonProperty("mark_info")]
        public MarkInfo MarkInfo { get; set; }

        [JsonProperty("model_info")]
        public ModelInfo ModelInfo { get; set; }

        [JsonProperty("super_gen")]
        public SuperGen SuperGen { get; set; }

        [JsonProperty("configuration")]
        public Configuration Configuration { get; set; }

        [JsonProperty("tech_param")]
        public TechParam TechParam { get; set; }

        [JsonProperty("vendor")]
        public string Vendor { get; set; }
    }
}
