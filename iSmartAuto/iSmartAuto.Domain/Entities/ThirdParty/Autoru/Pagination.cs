﻿using Newtonsoft.Json;

namespace iSmartAuto.Domain.Entities.ThirdParty.Autoru
{
    public class Pagination
    {
        [JsonProperty("page")]
        public long Page { get; set; }

        [JsonProperty("page_size")]
        public long PageSize { get; set; }

        [JsonProperty("total_offers_count")]
        public long TotalOffersCount { get; set; }

        [JsonProperty("total_page_count")]
        public long TotalPageCount { get; set; }
    }
}