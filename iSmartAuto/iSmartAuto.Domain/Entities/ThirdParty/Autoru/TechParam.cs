﻿using Newtonsoft.Json;

namespace iSmartAuto.Domain.Entities.ThirdParty.Autoru
{
    public class TechParam
    {
        [JsonProperty("engine_type")]
        public string EngineType { get; set; }

        [JsonProperty("gear_type")]
        public string GearType { get; set; }

        [JsonProperty("transmission")]
        public string Transmission { get; set; }

        [JsonProperty("power")]
        public long Power { get; set; }

        [JsonProperty("displacement")]
        public long Displacement { get; set; }

        [JsonProperty("fuel_rate")]
        public double FuelRate { get; set; }
    }
}