﻿using Newtonsoft.Json;

namespace iSmartAuto.Domain.Entities.ThirdParty.Autoru
{
    public class Documents
    {
        [JsonProperty("owners_number")]
        public int OwnersNumber { get; set; }

        [JsonProperty("year")]
        public int Year { get; set; }
    }
}