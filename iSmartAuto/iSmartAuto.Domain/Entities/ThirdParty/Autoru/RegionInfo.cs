﻿using Newtonsoft.Json;

namespace iSmartAuto.Domain.Entities.ThirdParty.Autoru
{
    public class RegionInfo
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}