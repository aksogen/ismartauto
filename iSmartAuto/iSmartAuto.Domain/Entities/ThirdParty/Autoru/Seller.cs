﻿using Newtonsoft.Json;

namespace iSmartAuto.Domain.Entities.ThirdParty.Autoru
{
    public class Seller
    {
        [JsonProperty("location")]
        public Location Location { get; set; }
    }
}