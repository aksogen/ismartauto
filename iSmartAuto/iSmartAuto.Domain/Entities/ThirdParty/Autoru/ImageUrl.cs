﻿using Newtonsoft.Json;

namespace iSmartAuto.Domain.Entities.ThirdParty.Autoru
{
    public class ImageUrl
    {
        [JsonProperty("sizes")]
        public Sizes Sizes { get; set; }
    }
}