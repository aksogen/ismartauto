﻿using Newtonsoft.Json;

namespace iSmartAuto.Domain.Entities.ThirdParty.Autoru
{
    public class Sizes
    {
        [JsonProperty("small")]
        public string Small { get; set; }

        [JsonProperty("1200x900n")]
        public string The1200X900N { get; set; }

        [JsonProperty("120x90")]
        public string The120X90 { get; set; }

        [JsonProperty("1200x900")]
        public string The1200X900 { get; set; }

        [JsonProperty("92x69")]
        public string The92X69 { get; set; }

        [JsonProperty("thumb_m")]
        public string ThumbM { get; set; }

        [JsonProperty("832x624")]
        public string The832X624 { get; set; }

        [JsonProperty("456x342")]
        public string The456X342 { get; set; }

        [JsonProperty("320x240")]
        public string The320X240 { get; set; }

        [JsonProperty("full")]
        public string Full { get; set; }
    }
}