﻿using Newtonsoft.Json;

namespace iSmartAuto.Domain.Entities.ThirdParty.Autoru
{
    public class SuperGen
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("price_segment")]
        public string PriceSegment { get; set; }
    }
}