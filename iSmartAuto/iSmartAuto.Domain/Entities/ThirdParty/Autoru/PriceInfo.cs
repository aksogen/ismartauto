﻿using Newtonsoft.Json;

namespace iSmartAuto.Domain.Entities.ThirdParty.Autoru
{
    public class PriceInfo
    {
        [JsonProperty("price")]
        public long Price { get; set; }
    }
}