﻿namespace iSmartAuto.Domain.Entities.CarOffer
{
    public class CarInfo
    {
        public string Mark { get; set; }
        public string MarkName { get; set; }
        public string Model { get; set; }
        public string ModelName { get; set; }
        public string Generation { get; set; }
        public string GenerationName { get; set; }

        public string FullModel
        {
            get
            {
                return $"{MarkName} {ModelName} {GenerationName}";
            }
        }
    }
}