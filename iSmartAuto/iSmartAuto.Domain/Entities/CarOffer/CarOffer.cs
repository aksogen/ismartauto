﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iSmartAuto.Domain.Entities.CarOffer
{
    public class CarOffer
    {
        public Guid Id { get; set; }
        public Uri Url { get; set; }
        public string ImageUrl { get; set; }

        public CarInfo CarInfo { get; set; }
        public TechInfo TechInfo { get; set; }
        public DocumentInfo DocumentInfo { get; set; }
        public StateInfo StateInfo { get; set; }
        public PriceInfo PriceInfo { get; set; }
        public SellerInfo SellerInfo { get; set; }
    }
}
