﻿namespace iSmartAuto.Domain.Entities.CarOffer
{
    public class TechInfo
    {
        public string BodyType { get; set; }
        public string EngineType { get; set; }
        public string TransmissionType { get; set; }
        public string GearType { get; set; }
        public double HorsePower { get; set; }
        public string EngineCapacity { get; set; }
        public double FuelRate { get; set; }
    }
}