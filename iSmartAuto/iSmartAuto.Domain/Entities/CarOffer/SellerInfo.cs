﻿namespace iSmartAuto.Domain.Entities.CarOffer
{
    public class SellerInfo
    {
        public string Type { get; set; }
        public string Region { get; set; }
    }
}