﻿namespace iSmartAuto.Domain.Entities.CarOffer
{
    public class DocumentInfo
    {
        public int OwnersNumber { get; set; }
        public int Year { get; set; }
    }
}