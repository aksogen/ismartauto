﻿using iSmartAuto.Domain.Enums.CarOffer;

namespace iSmartAuto.Domain.Entities.CarOffer
{
    public class PriceInfo
    {
        public PriceInfo()
        {
            PriceTagType = PriceTagType.Unpredicted;
        }

        public long ActualPrice { get; set; }
        public long? PredictedPrice { get; set; }
        public double? PredictedDifferencePercentage { get; set; }
        public PriceTagType PriceTagType { get; set; }
        public string PriceSegment { get; set; }
    }
}