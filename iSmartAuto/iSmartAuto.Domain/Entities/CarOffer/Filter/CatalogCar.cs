﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iSmartAuto.Domain.Entities.CarOffer.Filter
{
    public class CatalogCar
    {
        public string Mark { get; set; }
        public string Model { get; set; }
        public string Generation { get; set; }
    }
}
