﻿using iSmartAuto.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace iSmartAuto.Domain.Entities.CarOffer.Filter
{
    public class CarOfferFilter
    {
        public bool IsOnlyUsed { get; set; } = false;
        public bool IsOnlyUnbroken { get; set; } = true;

        public IEnumerable<CatalogCar> CatalogCarFilter { get; set; } = new List<CatalogCar>();

        public IEnumerable<string> Regions { get; set; }

        public IEnumerable<string> BodyTypes { get; set; }
        public IEnumerable<string> EngineTypes { get; set; }
        public IEnumerable<string> GearTypes { get; set; }
        public IEnumerable<string> TransmissionTypes { get; set; }

        public Range<int?> YearRange { get; set; }
        public Range<int?> HorsePowerRange { get; set; }
        public Range<string> PriceRange { get; set; }
        public Range<string> MileageRange { get; set; }
    }
}
