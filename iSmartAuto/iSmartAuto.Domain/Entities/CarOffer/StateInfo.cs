﻿namespace iSmartAuto.Domain.Entities.CarOffer
{
    public class StateInfo
    {
        public long Mileage { get; set; }
        public string Group { get; set; }
        public bool IsUnbrokenGroup { get; set; }
    }
}