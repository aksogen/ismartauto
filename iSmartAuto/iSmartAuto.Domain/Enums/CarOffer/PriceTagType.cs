﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iSmartAuto.Domain.Enums.CarOffer
{
    public enum PriceTagType
    {
        Unpredicted,
        GoodPrice,
        ExcellentPrice,
        HighPrice,
    }
}
