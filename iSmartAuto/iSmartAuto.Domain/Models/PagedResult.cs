﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iSmartAuto.Domain.Models
{
    public class PagedResult<T>
    {
        public PagedResult(IEnumerable<T> items, int page, int pageSize, long totalItemCount, long totalPageCount)
        {
            Items = items;
            Page = page;
            PageSize = pageSize;
            TotalItemCount = totalItemCount;
            TotalPageCount = totalPageCount;
        }

        public IEnumerable<T> Items { get; private set; }

        public int Page { get; set; }

        public int PageSize { get; set; }

        public long TotalItemCount { get; set; }

        public long TotalPageCount { get; set; }
    }
}
