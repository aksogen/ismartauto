﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iSmartAuto.Domain.Models
{
    public class Range<T>
    {
        public T Min { get; set; }
        public T Max { get; set; }
    }
}
