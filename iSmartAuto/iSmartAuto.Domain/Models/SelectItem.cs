﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iSmartAuto.Domain.Models
{
    public class SelectItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool IsTop { get; set; }
    }
}
