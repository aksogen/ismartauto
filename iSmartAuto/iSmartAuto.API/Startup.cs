using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using iSmartAuto.Domain.Interfaces.Services.CarOffers;
using iSmartAuto.Domain.Interfaces.Services.CarOffers.Prediction;
using iSmartAuto.Domain.Interfaces.Services.Catalog;
using iSmartAuto.Domain.Interfaces.Services.ThirdParty;
using iSmartAuto.Domain.Services.CarOffers;
using iSmartAuto.Domain.Services.CarOffers.Prediction;
using iSmartAuto.Domain.Services.Catalog;
using iSmartAuto.Domain.Services.DataCollection;
using iSmartAuto.Domain.Services.ThirdParty;
using iSmartAuto.Infrastructure.Configs.Mapper;
using iSmartAuto.Infrastructure.Configs.ML;
using iSmartAuto.Infrastructure.Configs.ThirdParty;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.ML;

namespace iSmartAuto.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(Startup), typeof(MapperProfile));
            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.AddConsole(options =>
                {
                    options.IncludeScopes = true;
                });
                loggingBuilder.AddDebug();
            });

            services.AddCors();
            services.AddControllers(options => 
            {
                options.AllowEmptyInputInBodyModelBinding = true;
            });

            services.AddApiVersioning(options =>
            {
                // Specify the default API Version
                options.DefaultApiVersion = new ApiVersion(1, 0);
                // Advertise the API versions supported for the particular endpoint
                options.ReportApiVersions = true;
            });


            services.Configure<AutoruConfig>(Configuration.GetSection(nameof(AutoruConfig)));
            services.Configure<MLCarOffersConfig>(Configuration.GetSection(nameof(MLCarOffersConfig)));

            services.AddSingleton<MLContext>();
            services.AddSingleton<DataCollectorService>();
            services.AddSingleton<IAutoruService, AutoruService>();
            services.AddSingleton<IPredictionModelService, PredictionModelService>();
            services.AddScoped<ICarPricePredictionService, CarPricePredictionService>();
            services.AddScoped<ICarOffersService, CarOffersService>();
            services.AddSingleton<ICarCatalogService, CarCatalogService>();
            services.AddSingleton<IRegionCatalogService, RegionCatalogService>();
        }
        

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder =>
                 builder.WithOrigins(new[] { "http://localhost:3000", "https://477ca9d9417b.ngrok.io" })
                    .AllowAnyMethod()
                    .AllowCredentials()
                    .AllowAnyHeader());

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
