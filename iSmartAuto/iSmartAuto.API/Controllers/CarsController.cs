﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iSmartAuto.Domain.Entities.CarOffer;
using iSmartAuto.Domain.Entities.CarOffer.Filter;
using iSmartAuto.Domain.Interfaces.Services.CarOffers;
using iSmartAuto.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace iSmartAuto.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class CarsController : ControllerBase
    {
        private readonly ILogger<CarsController> _logger;
        private readonly ICarOffersService _carOffersService;

        public CarsController(ILogger<CarsController> logger, ICarOffersService carOffersService)
        {
            _logger = logger;
            _carOffersService = carOffersService;
        }

        // POST: api/cars/search
        [HttpPost("search")]
        public async Task<ActionResult<PagedResult<CarOffer>>> SearchCars([FromBody]CarOfferFilter filter = null, 
            [FromQuery]int page = 1, [FromQuery]int pageSize = 20, [FromQuery]string sort = null)
        {
            var result = await _carOffersService.GetFilteredCarOffersAsync(filter, page, pageSize, sort);
            return Ok(result);
        }
    }
}
