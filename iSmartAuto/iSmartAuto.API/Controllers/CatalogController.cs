﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iSmartAuto.Domain.Interfaces.Services.Catalog;
using iSmartAuto.Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace iSmartAuto.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class CatalogController : ControllerBase
    {
        private readonly ILogger<CatalogController> _logger;
        private readonly IRegionCatalogService _regionCatalogService;
        private readonly ICarCatalogService _carCatalogService;

        public CatalogController(ILogger<CatalogController> logger, ICarCatalogService carCatalogService, IRegionCatalogService regionCatalogService)
        {
            _logger = logger;
            _carCatalogService = carCatalogService;
            _regionCatalogService = regionCatalogService;
        }

        // GET: api/catalog/cars/marks
        [HttpGet("cars/marks")]
        public ActionResult<IEnumerable<SelectItem>> GetCatalogCarsMarks()
        {
            var result = _carCatalogService.GetAllMarks();
            return Ok(result);
        }

        // GET: api/catalog/cars/{mark}/models
        [HttpGet("cars/{mark}/models")]
        public ActionResult<IEnumerable<SelectItem>> GetCatalogCarsMarkModels(string mark)
        {
            var result = _carCatalogService.GetModelsFromMark(mark);
            return Ok(result);
        }

        // GET: api/catalog/cars/{mark}/{model}
        [HttpGet("cars/{mark}/{model}")]
        public ActionResult<string> GetCatalogCarsModelGens(string mark, string model)
        {
            var result = _carCatalogService.GetGenerationsFromModel(mark, model);
            return Ok(result);
        }


        // GET: api/catalog/regions/cities
        [HttpGet("regions/cities")]
        public ActionResult<IEnumerable<SelectItem>> GetCatalogRegionCities()
        {
            var result = _regionCatalogService.GetAllCities();
            return Ok(result);
        }
    }
}
