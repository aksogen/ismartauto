﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iSmartAuto.Infrastructure.Configs.ML
{
    public class MLCarOffersModelConfig
    {
        public string GroupName { get; set; }
        public string DataPath { get; set; }
        public string PipelinePath { get; set; }
        public string ModelPath { get; set; }
        public int NumberOfLeaves { get; set; }
        public int NumberOfTrees { get; set; }
    }
}
