﻿using iSmartAuto.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace iSmartAuto.Infrastructure.Configs.ML
{
    public class MLCarOffersConfig
    {
        public Range<double> PriceRange { get; set; }
        public Range<double> YearRange { get; set; }
        public Range<double> OwnersRange { get; set; }
        public List<MLCarOffersModelConfig> ModelsConfig { get; set; }
    }
}
