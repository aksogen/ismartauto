﻿using System;
namespace iSmartAuto.Infrastructure.Configs.ThirdParty
{
    public class AutoruConfig
    {
        public string Credential { get; set; }
        public string UriScheme { get; set; }
        public string UriHost { get; set; }
        public string UriSearchCarsPath { get; set; }
        public int UriPort { get; set; }
    }
}
