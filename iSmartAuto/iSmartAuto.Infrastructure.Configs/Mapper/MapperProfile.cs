﻿using AutoMapper;
using iSmartAuto.Domain.Entities.ML;
using System;
using System.Collections.Generic;
using System.Text;

namespace iSmartAuto.Infrastructure.Configs.Mapper
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Domain.Entities.CarOffer.CarOffer, MLCar>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(e => e.Id.ToString()))
                .ForMember(dest => dest.Brand, opt => opt.MapFrom(e => e.CarInfo.Mark))
                .ForMember(dest => dest.Model, opt => opt.MapFrom(e => e.CarInfo.Model))
                .ForMember(dest => dest.Generation, opt => opt.MapFrom(e => e.CarInfo.Generation))
                .ForMember(dest => dest.BodyType, opt => opt.MapFrom(e => e.TechInfo.BodyType))
                .ForMember(dest => dest.EngineType, opt => opt.MapFrom(e => e.TechInfo.EngineType))
                .ForMember(dest => dest.TransmissionType, opt => opt.MapFrom(e => e.TechInfo.TransmissionType))
                .ForMember(dest => dest.GearType, opt => opt.MapFrom(e => e.TechInfo.GearType))
                .ForMember(dest => dest.PriceSegment, opt => opt.MapFrom(e => e.PriceInfo.PriceSegment))
                .ForMember(dest => dest.Power, opt => opt.MapFrom(e => float.Parse(e.TechInfo.HorsePower.ToString())))
                .ForMember(dest => dest.FuelRate, opt => opt.MapFrom(e => float.Parse(e.TechInfo.FuelRate.ToString())))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(e => float.Parse(e.PriceInfo.ActualPrice.ToString())))
                .ForMember(dest => dest.Owners, opt => opt.MapFrom(e => float.Parse(e.DocumentInfo.OwnersNumber.ToString())))
                .ForMember(dest => dest.Mileage, opt => opt.MapFrom(e => float.Parse(e.StateInfo.Mileage.ToString())))
                .ForMember(dest => dest.Year, opt => opt.MapFrom(e => float.Parse(e.DocumentInfo.Year.ToString())));
        }
    }
}
