﻿using iSmartAuto.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iSmartAuto.Domain.Interfaces.Services.Catalog
{
    public interface ICarCatalogService
    {
        IEnumerable<SelectItem> GetAllMarks();
        IEnumerable<SelectItem> GetModelsFromMark(string mark);
        IEnumerable<SelectItem> GetGenerationsFromModel(string mark, string model);
    }
}
