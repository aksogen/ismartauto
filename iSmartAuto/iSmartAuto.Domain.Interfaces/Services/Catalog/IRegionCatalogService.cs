﻿using iSmartAuto.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace iSmartAuto.Domain.Interfaces.Services.Catalog
{
    public interface IRegionCatalogService
    {
        IEnumerable<SelectItem> GetAllCities();
    }
}
