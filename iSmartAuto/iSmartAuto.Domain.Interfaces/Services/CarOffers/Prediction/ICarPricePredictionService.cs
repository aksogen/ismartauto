﻿using iSmartAuto.Domain.Entities.CarOffer;
using System;
using System.Collections.Generic;
using System.Text;

namespace iSmartAuto.Domain.Interfaces.Services.CarOffers.Prediction
{
    public interface ICarPricePredictionService
    {
        List<CarOffer> PredictPrices(List<CarOffer> carOffers);
    }
}
