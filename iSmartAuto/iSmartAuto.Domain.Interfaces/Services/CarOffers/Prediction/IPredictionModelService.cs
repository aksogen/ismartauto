﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iSmartAuto.Domain.Interfaces.Services.CarOffers.Prediction
{
    public interface IPredictionModelService
    {
        void CreatePredictionModels();
    }
}
