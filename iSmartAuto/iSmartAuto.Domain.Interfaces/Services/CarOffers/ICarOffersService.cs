﻿using iSmartAuto.Domain.Entities.CarOffer;
using iSmartAuto.Domain.Entities.CarOffer.Filter;
using iSmartAuto.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iSmartAuto.Domain.Interfaces.Services.CarOffers
{
    public interface ICarOffersService
    {
        Task<PagedResult<CarOffer>> GetFilteredCarOffersAsync(CarOfferFilter filter, int page, int pageSize, string sort);
    }
}
