﻿using iSmartAuto.Domain.Entities.CarOffer.Filter;
using iSmartAuto.Domain.Entities.ThirdParty.Autoru;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iSmartAuto.Domain.Interfaces.Services.ThirdParty
{
    public interface IAutoruService
    {
        Task<AutoruResponse> GetCarOffersPageAsync(CarOfferFilter filter, int page, int pageSize, string sort);
        Task<string> GetCarOffersPageStringifyAsync(int page, int pageSize);
    }
}
