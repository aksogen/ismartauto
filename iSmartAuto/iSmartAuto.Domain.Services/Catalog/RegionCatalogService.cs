﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using iSmartAuto.Domain.Interfaces.Services.Catalog;
using iSmartAuto.Domain.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace iSmartAuto.Domain.Services.Catalog
{
    public class RegionCatalogService : IRegionCatalogService
    {
        private readonly ILogger<RegionCatalogService> _logger;
        private Dictionary<string, dynamic> citiesDictionary;

        public RegionCatalogService(ILogger<RegionCatalogService> logger)
        {
            _logger = logger;

            var outPath = Path.Combine(Directory.GetCurrentDirectory(), "../iSmartAuto.Infrastructure.Persistence/");
            string jsonData = File.ReadAllText($"{outPath}/cities.json");
            citiesDictionary = JObject.Parse(jsonData).ToObject<Dictionary<string, dynamic>>();
        }

        public IEnumerable<SelectItem> GetAllCities()
        {
            List<SelectItem> result = new List<SelectItem>();

            foreach (var city in citiesDictionary)
            {
                result.Add(new SelectItem { Id = city.Value.id, Name = city.Value.name, IsTop = city.Value.popular });
            }

            return result;
        }
    }
}
