﻿using iSmartAuto.Domain.Interfaces.Services.CarOffers;
using iSmartAuto.Domain.Interfaces.Services.Catalog;
using iSmartAuto.Domain.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iSmartAuto.Domain.Services.Catalog
{
    public class CarCatalogService : ICarCatalogService
    {
        private readonly ILogger<CarCatalogService> _logger;
        private Dictionary<dynamic, dynamic> carsCatalogDictionary;

        public CarCatalogService(ILogger<CarCatalogService> logger)
        {
            _logger = logger;

            var outPath = Path.Combine(Directory.GetCurrentDirectory(), "../iSmartAuto.Infrastructure.Persistence/");
            string jsonData = File.ReadAllText($"{outPath}/marks-models-gen.json");
            carsCatalogDictionary = JObject.Parse(jsonData).ToObject<Dictionary<dynamic, dynamic>>();
        }

        public IEnumerable<SelectItem> GetAllMarks()
        {
            List<SelectItem> result = new List<SelectItem>();

            foreach (var mark in carsCatalogDictionary)
            {
                result.Add(new SelectItem { Id = mark.Value.id, Name = mark.Value.name, IsTop = mark.Value.popular });
            }

            return result;
        }

        public IEnumerable<SelectItem> GetModelsFromMark(string mark)
        {
            List<SelectItem> result = new List<SelectItem>();

            var requestedMark = carsCatalogDictionary.FirstOrDefault(e => e.Key == mark.ToUpper());

            if (requestedMark.Value == null)
            {
                return result;
            }

            foreach (var model in requestedMark.Value.models)
            {
                result.Add(new SelectItem { Id = model.Value.id, Name = model.Value.name });
            }

            return result;
        }

        public IEnumerable<SelectItem> GetGenerationsFromModel(string mark, string model)
        {
            List<SelectItem> result = new List<SelectItem>();

            var requestedMark = carsCatalogDictionary.FirstOrDefault(e => e.Key == mark.ToUpper());

            if (requestedMark.Value == null)
            {
                return result;
            }

            foreach (var m in requestedMark.Value.models)
            {
                if (m.Value.id == model.ToUpper())
                {
                    foreach (var gen in m.Value.generations)
                    {
                        result.Add(new SelectItem { Id = gen.Value.id, Name = gen.Value.name });
                    }
                }
            }

            return result;
        }
    }
}
