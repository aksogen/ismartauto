﻿using iSmartAuto.Domain.Entities.CarOffer.Filter;
using iSmartAuto.Domain.Entities.ThirdParty.Autoru;
using iSmartAuto.Domain.Interfaces.Services.ThirdParty;
using iSmartAuto.Infrastructure.Configs.ThirdParty;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace iSmartAuto.Domain.Services.ThirdParty
{
    public class AutoruService : IAutoruService
    {
        private readonly ILogger<AutoruService> _logger;
        private readonly AutoruConfig _autoruConfig;
        private readonly HttpClient _httpClient;

        public AutoruService(ILogger<AutoruService> logger, IOptions<AutoruConfig> autoruConfig)
        {
            _logger = logger;
            _autoruConfig = autoruConfig.Value;
            _httpClient = HttpClientFactory.Create();
            _httpClient.DefaultRequestHeaders.Add("X-Authorization", _autoruConfig.Credential);
        }

        public async Task<AutoruResponse> GetCarOffersPageAsync(CarOfferFilter filter, int page, int pageSize, string sort)
        {
            UriBuilder uriBuilder = new UriBuilder(_autoruConfig.UriScheme, _autoruConfig.UriHost,
                _autoruConfig.UriPort, _autoruConfig.UriSearchCarsPath, $"?page={page}&page_size={pageSize}");

            uriBuilder = ApplySort(uriBuilder, sort);
            Uri uri = uriBuilder.Uri;
            string bodyRequest = ApplyFilter(filter);

            _logger.LogInformation($"POST request to {uri.ToString()}");
            var response = await _httpClient.PostAsync(uri, new StringContent(bodyRequest, Encoding.UTF8, "application/json"));

            if (!response.IsSuccessStatusCode)
            {
                _logger.LogError($"apiauto.ru response: {response.StatusCode}");
                throw new Exception();
            }

            _logger.LogInformation($"apiauto.ru response: {response.StatusCode}");
            var result = await response.Content.ReadAsAsync<AutoruResponse>();

            if (result.CarOffers == null)
            {
                result.CarOffers = new List<CarOffer>();
            }

            return result;
        }

        public async Task<string> GetCarOffersPageStringifyAsync(int page, int pageSize)
        {
            UriBuilder uriBuilder = new UriBuilder(_autoruConfig.UriScheme, _autoruConfig.UriHost,
                _autoruConfig.UriPort, _autoruConfig.UriSearchCarsPath, $"?page={page}&page_size={pageSize}");
            Uri uri = uriBuilder.Uri;

            dynamic autoruFilter = new
            {
                damage_group = "NOT_BEATEN",
                in_stock = "ANY_STOCK",
                state_group = "USED",
            };

            string bodyRequest = JsonConvert.SerializeObject(autoruFilter, Formatting.Indented, new JsonSerializerSettings
            {
                DefaultValueHandling = DefaultValueHandling.Ignore
            });

            var response = await _httpClient.PostAsync(uri, new StringContent(bodyRequest, Encoding.UTF8, "application/json"));

            if (!response.IsSuccessStatusCode)
            {
                _logger.LogError($"apiauto.ru response: {response.StatusCode}");
                throw new Exception();
            }

            _logger.LogInformation($"apiauto.ru response: {response.StatusCode}");
            var result = await response.Content.ReadAsStringAsync();

            return result;
        }

        private UriBuilder ApplySort(UriBuilder uriBuilder, string sort)
        {
            if (sort == null)
            {
                return uriBuilder;
            }

            string autoruSort;
            switch (sort)
            {
                case "price-asc":
                    autoruSort = "price-asc";
                    break;
                case "price-desc":
                    autoruSort = "price-desc";
                    break;
                case "year-asc":
                    autoruSort = "year-asc";
                    break;
                case "year-desc":
                    autoruSort = "year-desc";
                    break;
                case "mileage-asc":
                    autoruSort = "km_age-asc";
                    break;
                case "mileage-desc":
                    autoruSort = "km_age-desc";
                    break;
                default:
                    autoruSort = null;
                    break;
            }

            if (autoruSort == null)
            {
                return uriBuilder;
            }

            uriBuilder.Query = $"{uriBuilder.Query}&sort={autoruSort}";

            return uriBuilder;
        }

        private string ApplyFilter(CarOfferFilter filter) 
        {
            dynamic autoruFilter = new
            {
                in_stock = "ANY_STOCK",
                state_group = "USED",
                damage_group = "NOT_BEATEN",
                has_image = true,
            };

            if (filter != null)
            {
                List<object> catalogCarFilter = new List<object>();

                foreach (var item in filter.CatalogCarFilter)
                {
                    catalogCarFilter.Add(new
                    {
                        mark = item.Mark ?? null,
                        model = item.Model ?? null,
                        generation = item.Generation ?? null,
                    });
                }

                autoruFilter = new
                {
                    in_stock = "ANY_STOCK",
                    state_group = filter.IsOnlyUsed ? "USED" : "ALL",
                    damage_group = filter.IsOnlyUnbroken ? "NOT_BEATEN" : "ANY",
                    has_image = true,

                    catalog_filter = catalogCarFilter,

                    rid = filter.Regions ?? null,

                    body_type_group = filter.BodyTypes ?? null,
                    engine_group = filter.EngineTypes ?? null,
                    gear_type = filter.GearTypes ?? null,
                    transmission = filter.TransmissionTypes ?? null,

                    year_from = filter.YearRange?.Min ?? null,
                    year_to = filter.YearRange?.Max ?? null,
                    power_from = filter.HorsePowerRange?.Min ?? null,
                    power_to = filter.HorsePowerRange?.Max ?? null,
                    price_from = filter.PriceRange?.Min ?? null,
                    price_to = filter.PriceRange?.Max ?? null,
                    km_age_from = filter.MileageRange?.Min ?? null,
                    km_age_to = filter.MileageRange?.Max ?? null,
                };
            }

            string jsonAutoruFilterResult = JsonConvert.SerializeObject(autoruFilter, Formatting.Indented, new JsonSerializerSettings
            {
                DefaultValueHandling = DefaultValueHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            });

            return jsonAutoruFilterResult;
        }
    }
}
