﻿using iSmartAuto.Domain.Entities.ThirdParty.Autoru;
using iSmartAuto.Domain.Interfaces.Services.ThirdParty;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace iSmartAuto.Domain.Services.DataCollection
{
    public class DataCollectorService
    {
        private readonly ILogger<DataCollectorService> _logger;
        private readonly IAutoruService _autoruService;
        private string _outPath;
        private int _pageCount;
        private int _pageSize;

        private string _guid;
        private string _date;

        public DataCollectorService(ILogger<DataCollectorService> logger, IAutoruService autoruService)
        {
            _logger = logger;
            _autoruService = autoruService;
            _outPath = Directory.GetCurrentDirectory() + "../../../../../iSmartAuto.Infrastructure.Persistence/Data";
            _pageCount = 6000;
            _pageSize = 100;

            _guid = "703eca77";   // f795cf31    // 703eca77    // c3b29591    
            _date = "04.12.2020"; // 02.08.2020  // 04.12.2020  // 06.06.2020  
        }

        public async Task CollectJsonAsync()
        {
            //Random rnd = new Random();
            Directory.CreateDirectory(Path.Combine(_outPath, $"dump-{_date}-{_guid}/json"));

            for (int i = 1; i <= _pageCount; i++)
            {
                //System.Threading.Thread.Sleep(rnd.Next(1000, 2000));

                var result = await _autoruService.GetCarOffersPageStringifyAsync(i, _pageSize);

                using (StreamWriter outputFile = new StreamWriter(Path.Combine(_outPath, $"dump-{_date}-{_guid}/json/data-{i}.json")))
                {
                    await outputFile.WriteAsync(result);
                }
                _logger.LogInformation($"dump-{_date}-{_guid}/json/data-{i}.json was successfully saved");
            }
        }

        public void ParseJsonToCsv()
        {
            Directory.CreateDirectory(Path.Combine(_outPath, $"dump-{_date}-{_guid}/csv"));

            using StreamWriter outputDataFile = new StreamWriter(Path.Combine(_outPath, $"dump-{_date}-{_guid}/csv", "car_offers_all_data.csv"), false);
            using StreamWriter outputDataEFile = new StreamWriter(Path.Combine(_outPath, $"dump-{_date}-{_guid}/csv", "car_offers_economy_data.csv"), false);
            using StreamWriter outputDataMFile = new StreamWriter(Path.Combine(_outPath, $"dump-{_date}-{_guid}/csv", "car_offers_medium_data.csv"), false);
            using StreamWriter outputDataPFile = new StreamWriter(Path.Combine(_outPath, $"dump-{_date}-{_guid}/csv", "car_offers_premium_data.csv"), false);

            outputDataFile.WriteLine("mark;model;generation;bodyType;engineType;transmissionType;gearType;priceSegment;power;fuelRate;price;owners;year;mileage");
            outputDataEFile.WriteLine("mark;model;generation;bodyType;engineType;transmissionType;gearType;priceSegment;power;fuelRate;price;owners;year;mileage");
            outputDataMFile.WriteLine("mark;model;generation;bodyType;engineType;transmissionType;gearType;priceSegment;power;fuelRate;price;owners;year;mileage");
            outputDataPFile.WriteLine("mark;model;generation;bodyType;engineType;transmissionType;gearType;priceSegment;power;fuelRate;price;owners;year;mileage");

            var jsonsCount = Directory.GetFiles(Path.Combine(_outPath, $"dump-{_date}-{_guid}/json")).Length;

            for (int i = 1; i <= jsonsCount; i++)
            {
                string line = string.Empty;
                string jsonData = File.ReadAllText(Path.Combine(_outPath, $"dump-{_date}-{_guid}/json/data-{i}.json"));
                AutoruResponse response = JsonConvert.DeserializeObject<AutoruResponse>(jsonData);

                foreach (var offer in response.CarOffers)
                {

                    line = String.Join(";", new string[]
                    {
                            offer.CarInfo.Mark ?? string.Empty,
                            offer.CarInfo.Model ?? string.Empty,
                            offer.CarInfo.SuperGen?.Name?.Replace(";", ",") ?? string.Empty,
                            offer.CarInfo.BodyType ?? string.Empty,
                            offer.CarInfo.EngineType ?? string.Empty,
                            offer.CarInfo.Transmission ?? string.Empty,
                            offer.CarInfo.TechParam?.GearType ?? string.Empty,
                            offer.CarInfo.SuperGen?.PriceSegment ?? string.Empty,
                            offer.CarInfo.HorsePower.ToString(),
                            offer.CarInfo.TechParam?.FuelRate.ToString().Replace(',', '.') ?? string.Empty,
                            offer.PriceInfo?.Price.ToString() ?? string.Empty,
                            offer.Documents?.OwnersNumber.ToString() ?? string.Empty,
                            offer.Documents?.Year.ToString() ?? string.Empty,
                            offer.State?.Mileage.ToString() ?? string.Empty
                    });

                    outputDataFile.WriteLine(line);

                    switch (offer.CarInfo.SuperGen?.PriceSegment)
                    {
                        case "ECONOMY":
                            outputDataEFile.WriteLine(line);
                            break;
                        case "MEDIUM":
                            outputDataMFile.WriteLine(line);
                            break;
                        case "PREMIUM":
                            outputDataPFile.WriteLine(line);
                            break;
                    }
                }

                _logger.LogInformation($"json-{i}-parsed");
            }
        }
    }
}
