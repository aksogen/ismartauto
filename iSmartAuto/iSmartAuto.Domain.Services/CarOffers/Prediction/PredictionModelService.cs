﻿using iSmartAuto.Domain.Entities.ML;
using iSmartAuto.Domain.Interfaces.Services.CarOffers.Prediction;
using iSmartAuto.Infrastructure.Configs.ML;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.ML;
using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace iSmartAuto.Domain.Services.CarOffers.Prediction
{
    public class PredictionModelService : IPredictionModelService
    {
        private readonly ILogger<PredictionModelService> _logger;
        private readonly MLContext _mlContext;
        private readonly MLCarOffersConfig _mlCarOffersConfig;
        static readonly string backPath = "../../../../iSmartAuto.Infrastructure.Persistence";

        private string _guid;
        private string _date;

        public PredictionModelService(ILogger<PredictionModelService> logger, MLContext mlContext, IOptions<MLCarOffersConfig> mlCarOffersConfig)
        {
            _logger = logger;
            _mlContext = mlContext;
            _mlCarOffersConfig = mlCarOffersConfig.Value;

            _guid = "703eca77"; // 703eca77 / c3b29591
            _date = "04.12.2020"; // 04.12.2020 / 06.06.2020
        }

        public void CreatePredictionModels()
        {
            // Create ML models for each car offers subset by price segment (economy, medium, premium)
            foreach (var mlCarOffersModelConfig in _mlCarOffersConfig.ModelsConfig)
            {
                var carOffersDatalPath = Path.Combine(Environment.CurrentDirectory, backPath, $"Data/dump-{_date}-{_guid}/csv", mlCarOffersModelConfig.DataPath);
                var carOffersPrepTransformerPath = Path.Combine(Environment.CurrentDirectory, backPath, "MLModels", mlCarOffersModelConfig.PipelinePath);
                var carOffersMLModelPath = Path.Combine(Environment.CurrentDirectory, backPath, "MLModels", mlCarOffersModelConfig.ModelPath);

                TrainAndSaveModel(carOffersDatalPath, carOffersPrepTransformerPath, carOffersMLModelPath, 
                    mlCarOffersModelConfig.NumberOfLeaves, mlCarOffersModelConfig.NumberOfTrees);
            }
        }

        private void TrainAndSaveModel(string dataPath, string dataPrepTransformerPath, string carOffersModelPath, int nLeaves, int nTrees)
        {
            _logger.LogInformation("Load raw data from csv-file");
            IDataView rawData = _mlContext.Data.LoadFromTextFile<MLCar>(dataPath, hasHeader: true, separatorChar: ';');
            //var g = rawData.Preview();

            _logger.LogInformation("Filter raw data");
            // Filter raw data to rid of noise

            IDataView filteredData = _mlContext.Data
                .FilterRowsByColumn(rawData, nameof(MLCar.Price), upperBound: _mlCarOffersConfig.PriceRange.Max);
            filteredData = _mlContext.Data
                .FilterRowsByColumn(filteredData, nameof(MLCar.Year), lowerBound: _mlCarOffersConfig.YearRange.Min, upperBound: _mlCarOffersConfig.YearRange.Max);
            filteredData = _mlContext.Data
                .FilterRowsByColumn(filteredData, nameof(MLCar.Owners), lowerBound: _mlCarOffersConfig.OwnersRange.Min);

            _logger.LogInformation("Data preparations");
            IEstimator<ITransformer> dataPrepEstimator =
                _mlContext.Transforms.CopyColumns(outputColumnName: "Label", inputColumnName: "Price")
                    .Append(_mlContext.Transforms.Concatenate("FullModel", nameof(MLCar.Brand), nameof(MLCar.Model), nameof(MLCar.Generation)))
                    // Since this is a regression model, we apply one hot encoding onto categorical values to turn them into numerical values
                    .Append(_mlContext.Transforms.Categorical.OneHotEncoding(outputColumnName: "FullModelEncoded", inputColumnName: "FullModel"))
                    .Append(_mlContext.Transforms.Categorical.OneHotEncoding(outputColumnName: "BodyEncoded", inputColumnName: nameof(MLCar.BodyType)))
                    .Append(_mlContext.Transforms.Categorical.OneHotEncoding(outputColumnName: "EngineCodeEncoded", inputColumnName: nameof(MLCar.EngineType)))
                    .Append(_mlContext.Transforms.Categorical.OneHotEncoding(outputColumnName: "TransmissionEncoded", inputColumnName: nameof(MLCar.TransmissionType)))
                    .Append(_mlContext.Transforms.Categorical.OneHotEncoding(outputColumnName: "GearEncoded", inputColumnName: nameof(MLCar.GearType)))
                    // Replace missing values.
                    //.Append(mlContext.Transforms.ReplaceMissingValues(nameof(Car.FuelRate), replacementMode: MissingValueReplacingEstimator.ReplacementMode.Mean))
                    // Group all predictors (regressors) into array called "features".
                    .Append(_mlContext.Transforms.Concatenate("Features", nameof(MLCar.Mileage), nameof(MLCar.Year), nameof(MLCar.Power), nameof(MLCar.Owners),
                        nameof(MLCar.FuelRate), "FullModelEncoded", "BodyEncoded", "EngineCodeEncoded", "TransmissionEncoded", "GearEncoded"))
                    // Normilize features - rescale to be between -1 and 1 for all examples.
                    .Append(_mlContext.Transforms.NormalizeMinMax("Features", fixZero: true));

            // Create data prep transformer
            ITransformer dataPrepTransformer = dataPrepEstimator.Fit(filteredData);
            // Transform data
            IDataView transformedData = dataPrepTransformer.Transform(filteredData);
            //var h = transformedData.Preview();

            _logger.LogInformation("Save data preparations Transformer");
            using (var fileStream = new FileStream(dataPrepTransformerPath, FileMode.Create, FileAccess.Write, FileShare.Write))
            {
                _mlContext.Model.Save(dataPrepTransformer, filteredData.Schema, fileStream);
            }


            _logger.LogInformation("Train model with cross validation");
            // Define algorithm estimator
            IEstimator<ITransformer> fastTreeEstimator = _mlContext.Regression.Trainers.FastTree(numberOfLeaves: nLeaves, numberOfTrees: nTrees);
            // Apply 5-fold cross validation
            var cvResults = _mlContext.Regression.CrossValidate(transformedData, fastTreeEstimator, numberOfFolds: 5);

            IEnumerable<RegressionMetrics> metrics = cvResults.Select(fold => fold.Metrics);
            foreach (var metrica in metrics)
            {
                Console.WriteLine($"{metrica.RSquared} - {metrica.RootMeanSquaredError}");
            }

            // Select all models
            ITransformer[] models =
                cvResults
                    .OrderByDescending(fold => fold.Metrics.RSquared)
                    .Select(fold => fold.Model)
                    .ToArray();

            // Get the best ml model
            ITransformer bestModel = models[0];

            // Extract the tree parameters.
            var fastTreeRegressionModelParameters = (RegressionPredictionTransformer<Microsoft.ML.Trainers.FastTree.FastTreeRegressionModelParameters>) bestModel;
            var treeCollection = fastTreeRegressionModelParameters.Model.TrainedTreeEnsemble;

            _logger.LogInformation("Save Model");
            using (var fileStream = new FileStream(carOffersModelPath, FileMode.Create, FileAccess.Write, FileShare.Write))
            {
                _mlContext.Model.Save(bestModel, transformedData.Schema, fileStream);
            }
        }
    }
}
