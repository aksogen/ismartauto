﻿using AutoMapper;
using iSmartAuto.Domain.Entities.CarOffer;
using iSmartAuto.Domain.Entities.ML;
using iSmartAuto.Domain.Enums.CarOffer;
using iSmartAuto.Domain.Interfaces.Services.CarOffers.Prediction;
using iSmartAuto.Infrastructure.Configs.ML;
using Microsoft.Extensions.Options;
using Microsoft.ML;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace iSmartAuto.Domain.Services.CarOffers.Prediction
{
    public class CarPricePredictionService : ICarPricePredictionService
    {
        private readonly IMapper _mapper;
        private readonly MLContext _mlContext;
        private readonly MLCarOffersConfig _mlCarOffersConfig;
        static readonly string backPath = "../iSmartAuto.Infrastructure.Persistence";

        public CarPricePredictionService(IMapper mapper, MLContext mlContext, IOptions<MLCarOffersConfig> mlCarOffersConfig)
        {
            _mapper = mapper;
            _mlCarOffersConfig = mlCarOffersConfig.Value;
            _mlContext = mlContext;
        }

        public List<CarOffer> PredictPrices(List<CarOffer> carOffers)
        {
            var filteredCarOffers = carOffers
                .Where(e => e.PriceInfo.ActualPrice < _mlCarOffersConfig.PriceRange.Max)
                .Where(e => e.DocumentInfo.Year > _mlCarOffersConfig.YearRange.Min)
                .Where(e => e.StateInfo.Group == "USED")
                .Where(e => e.StateInfo.IsUnbrokenGroup == true)
                .ToList();

            if (filteredCarOffers == null)
            {
                return carOffers;
            }

            foreach (var mlCarOffersModelConfig in _mlCarOffersConfig.ModelsConfig)
            {
                var carOffersPrepTransformerPath = Path.Combine(Environment.CurrentDirectory, backPath, "MLModels", mlCarOffersModelConfig.PipelinePath);
                var carOffersMLModelPath = Path.Combine(Environment.CurrentDirectory, backPath, "MLModels", mlCarOffersModelConfig.ModelPath);

                // Create new File Streams for data preparation pipeline and ML model
                using var carOffersPipelineStream = new FileStream(carOffersPrepTransformerPath, FileMode.Open, FileAccess.Read, FileShare.Read);
                using var carOffersMLModelStream = new FileStream(carOffersMLModelPath, FileMode.Open, FileAccess.Read, FileShare.Read);
                // Load the data preparation pipeline and ML model from the files
                ITransformer carOffersPipeline = _mlContext.Model.Load(carOffersPipelineStream, out DataViewSchema carOffersPrepPipelineSchema);
                ITransformer carOffersMLModel = _mlContext.Model.Load(carOffersMLModelStream, out DataViewSchema carOffersMLModelSchema);

                // Split car offers into sub-list by price segment of car offer
                var carOffersSegment = filteredCarOffers.Where(e => e.PriceInfo.PriceSegment == mlCarOffersModelConfig.GroupName).ToList();
                // MAP: List<CarOffer> -> List<MLCar> using AutoMapper
                // See Infrastructure.Configs.Mapper.MapperProfile for more details
                var mlCarOffersSegment = _mapper.Map<List<CarOffer>, List<MLCar>>(carOffersSegment);
                // Create IDataView from sub-list, because ML model only work with this type of data
                IDataView mlCarOffersSegmentDataView = _mlContext.Data.LoadFromEnumerable(mlCarOffersSegment);

                // Predict the price for each car in the sub-list of car offers
                IDataView mlPredictedCarOffersSegmentDataView = carOffersMLModel.Transform(carOffersPipeline.Transform(mlCarOffersSegmentDataView));

                // Create list of MLCarPricePrediction from IDataView with predicted prices for current price segment
                List<MLCarPricePrediction> mlPredictedCarOffersSegment =
                    _mlContext.Data.CreateEnumerable<MLCarPricePrediction>(mlPredictedCarOffersSegmentDataView, reuseRowObject: false).ToList();


                foreach (var mlPredictedCarOffer in mlPredictedCarOffersSegment)
                {
                    var carOffer = carOffers.Where(e => e.Id.ToString() == mlPredictedCarOffer.Id).FirstOrDefault();

                    var predictedPrice = long.Parse(Math.Round(mlPredictedCarOffer.Price).ToString());
                    var predictedDifferencePercentage = DifferencePercentage(carOffer.PriceInfo.ActualPrice, predictedPrice);

                    carOffer.PriceInfo.PredictedPrice = predictedPrice;
                    carOffer.PriceInfo.PredictedDifferencePercentage = predictedDifferencePercentage;
                    carOffer.PriceInfo.PriceTagType = IdentifyCarOfferPriceTag(predictedDifferencePercentage);
                }
            }

            return carOffers;
        }

        private double DifferencePercentage(long firstValue, long secondValue)
        {
            var result = Math.Round((double.Parse(firstValue.ToString()) / double.Parse(secondValue.ToString())) * 100 - 100, 2);

            return result;
        }

        private PriceTagType IdentifyCarOfferPriceTag(double differencePercentage)
        {
            if (differencePercentage >= -10 && differencePercentage < 0)
            {
                return PriceTagType.ExcellentPrice;
            }
            else if (differencePercentage >= 0 && differencePercentage < 15)
            {
                return PriceTagType.GoodPrice;
            }
            else
            {
                return PriceTagType.HighPrice;
            }
        }
    }
}