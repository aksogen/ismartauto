﻿using iSmartAuto.Domain.Entities.CarOffer;
using iSmartAuto.Domain.Entities.CarOffer.Filter;
using iSmartAuto.Domain.Interfaces.Services.CarOffers;
using iSmartAuto.Domain.Interfaces.Services.CarOffers.Prediction;
using iSmartAuto.Domain.Interfaces.Services.ThirdParty;
using iSmartAuto.Domain.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iSmartAuto.Domain.Services.CarOffers
{
    public class CarOffersService : ICarOffersService
    {
        private readonly ILogger<CarOffersService> _logger;
        private readonly IAutoruService _autoruService;
        private readonly ICarPricePredictionService _carPricePredictionService;

        public CarOffersService(ILogger<CarOffersService> logger, IAutoruService autoruService, ICarPricePredictionService carPricePredictionService)
        {
            _logger = logger;
            _autoruService = autoruService;
            _carPricePredictionService = carPricePredictionService;
        }

        public async Task<PagedResult<CarOffer>> GetFilteredCarOffersAsync(CarOfferFilter filter, int page, int pageSize, string sort)
        {
            var autoruResponse = await _autoruService.GetCarOffersPageAsync(filter, page, pageSize, sort);

            // MAP: AUTORU.List<CarOffer> -> DOMAIN.List<CarOffer>
            var carOffers = new List<CarOffer>();
            foreach (var autoruCarOffer in autoruResponse.CarOffers)
            {
                carOffers.Add(new CarOffer()
                {
                    Id = Guid.NewGuid(),
                    Url = autoruCarOffer.Url,
                    ImageUrl = autoruCarOffer.State.ImageUrls[0].Sizes.The832X624,

                    CarInfo = new CarInfo()
                    {
                        Mark = autoruCarOffer.CarInfo.Mark,
                        MarkName = autoruCarOffer.CarInfo.MarkInfo.Name,
                        Model = autoruCarOffer.CarInfo.Model,
                        ModelName = autoruCarOffer.CarInfo.ModelInfo.Name,
                        Generation = autoruCarOffer.CarInfo.SuperGen?.Name?.Replace(";", ",") ?? string.Empty,
                        GenerationName = autoruCarOffer.CarInfo.SuperGen?.Name,
                    },

                    TechInfo = new TechInfo()
                    {
                        BodyType = autoruCarOffer.CarInfo.BodyType ?? string.Empty,
                        EngineType = autoruCarOffer.CarInfo.EngineType ?? string.Empty,
                        TransmissionType = autoruCarOffer.CarInfo.Transmission ?? string.Empty,
                        GearType = autoruCarOffer.CarInfo.TechParam?.GearType ?? string.Empty,
                        HorsePower = double.Parse(autoruCarOffer.CarInfo.HorsePower.ToString()),
                        EngineCapacity = string.Format("{0:0.0}", float.Parse(autoruCarOffer.CarInfo.TechParam?.Displacement.ToString()) / 1000).Replace(',', '.'),
                        FuelRate = Math.Round(double.Parse(autoruCarOffer.CarInfo.TechParam?.FuelRate.ToString() ?? string.Empty), 2),
                    },

                    PriceInfo = new PriceInfo()
                    {
                        ActualPrice = long.Parse(autoruCarOffer.PriceInfo?.Price.ToString() ?? "0"),
                        PriceSegment = autoruCarOffer.CarInfo.SuperGen?.PriceSegment ?? string.Empty,
                    },

                    DocumentInfo = new DocumentInfo()
                    {
                        OwnersNumber = int.Parse(autoruCarOffer.Documents?.OwnersNumber.ToString() ?? "1"),
                        Year = int.Parse(autoruCarOffer.Documents?.Year.ToString() ?? string.Empty),
                    },

                    StateInfo = new StateInfo()
                    {
                        Mileage = long.Parse(autoruCarOffer.State?.Mileage.ToString() ?? string.Empty),
                        Group = autoruCarOffer.Section ?? string.Empty,
                        IsUnbrokenGroup = autoruCarOffer.State?.StateNotBeaten ?? false,
                    },

                    SellerInfo = new SellerInfo()
                    {
                        Type = autoruCarOffer.SellerType,
                        Region = autoruCarOffer.Seller.Location.RegionInfo.Name,
                    }
                });
            }

            var predictedCarOffers = _carPricePredictionService.PredictPrices(carOffers);

            var result = new PagedResult<CarOffer>(predictedCarOffers, page, pageSize, 
                autoruResponse.Pagination.TotalOffersCount, autoruResponse.Pagination.TotalPageCount);

            return result;
        }
    }
}
