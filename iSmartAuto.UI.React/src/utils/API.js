import axios from 'axios';

export default axios.create({
  baseURL: `http://localhost:57767/api/v1.0/`,
  //baseURL: `https://c33efda430f6.ngrok.io/api/v1.0/`,
  responseType: "json",
  headers: {
    "Content-Type": "application/json, text/plain, */*"
  }
});