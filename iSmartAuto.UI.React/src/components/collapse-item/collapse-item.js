import React from 'react';
import { Collapse } from 'react-bootstrap';

export default class CollapseItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: this.props.open,
        };
    }   

    render() {
        return (
            <div className="collapse-item">
                <div className="trigger">
                    <div className="trigger-inner d-flex justify-content-between align-items-center"
                            onClick={() => this.setState({isOpen: !this.state.isOpen})}
                            aria-controls={this.props.target}
                            aria-expanded={this.state.isOpen}>
                        <h5 className="trigger-text">{this.props.title}</h5>
                        <div className="trigger-icon">
                            <i className="icon icon-arrow-down"></i>
                        </div>
                    </div>
                </div>
                <Collapse in={this.state.isOpen}>
                    <div id={this.props.target} className="content">
                        <div className="content-inner">
                            {this.props.children}
                        </div>
                    </div>
                </Collapse>
            </div>
        );
    }
}


