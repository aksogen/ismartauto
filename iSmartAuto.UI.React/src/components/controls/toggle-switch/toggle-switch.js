import React from 'react';
import './toggle-switch.scss'

export default class ToggleSwitch extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            checked: this.props.checked,
        }

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.setState({
            checked: !this.state.checked,
        });
        this.props.onChange(e.target.name, e.target.checked)
    }

    render() {
        return (
            <div class="toggle_switch">
                <label class="toggle_switch__label">
                    <input 
                        name={this.props.name}
                        class={"toggle_switch__input"}
                        type="checkbox"
                        onChange={this.handleChange} 
                        checked={this.state.checked}
                        disabled={this.props.disabled}
                    />
                    <div class="toggle_switch__presentation">
                        <div class="toggle_switch__states">
                            <span class="toggle_switch__state"></span>
                        </div>
                        <div class="toggle_switch__label-text">{this.props.label}</div>
                    </div>
                </label>
            </div>
        );
    }
}