import React from 'react';
import $ from 'jquery';
import './bootstrap-select.scss'

export default class BootstrapSelect extends React.Component {
    static defaultProps = {
        disabled: false,
        multiple: false,
        liveSearch: false,
        selectTextFormat: "values",
    }

    componentDidMount() {
        this.$el = $(this.el);
        this.$el.selectpicker();

        this.handleChange = this.handleChange.bind(this);
        this.$el.on('changed.bs.select', this.handleChange);
    }
      
    componentWillUnmount() {
        this.$el.off('changed.bs.select', this.handleChange);
        this.$el.selectpicker('destroy');
    }

    componentDidUpdate(prevProps) {
        if (prevProps.children !== this.props.children
            || prevProps.disabled !== this.props.disabled) {
            this.$el.selectpicker('refresh');
        }
    }

    handleChange(e) {
        this.props.onChange(e.target.name, this.$el.selectpicker('val'));
    }

    render() {
        return (
            <select 
                name={this.props.name}
                className="form-control" 
                ref={el => this.el = el} 
                title={this.props.title} 
                data-dropup-auto="false" 
                disabled={this.props.disabled}
                multiple={this.props.multiple}
                data-size={7}
                data-live-search={this.props.liveSearch}
                data-selected-text-format={this.props.selectTextFormat}
                >

                {this.props.children}
            </select>
        );
    }
}


