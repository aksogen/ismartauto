import React from 'react';
import { 
  Route,
  Switch,
  Redirect,
  withRouter
} from "react-router-dom"
import './app.scss';
import Home from './../home/home'
import Error from '../error/error'


class App extends React.Component {

  render() {
    const { history } = this.props

    return (
      <div className="app">
        <Switch>
          <Route history={history} exact path='/'>
            <Home history={history} />
          </Route>
          <Route history={history} path='/500'>
            <Error 
              errorCode={500}
              errorReason={'Уупс! Внутренняя ошибка сервера!'}
              errorText={'Сервис недоступен, проверьте соединение с интернетом или попробуйте обновить страницу позже.'}
            />
          </Route>
          <Route>
            <Error 
              errorCode={404}
              errorReason={'Уупс! Страница не найдена!'}
              errorText={'Страница, которую вы ищете, возможно, была бы удалена, или ее название было изменено.'}
            />
          </Route>
          <Redirect exact from='/' to='/'/>
        </Switch>
      </div>
    );
  }
  
}

export default withRouter(App)