import React from 'react';
import API from "./../../../../utils/API";
import { Col, Row } from 'react-bootstrap';
import BootstrapSelect from '../../../controls/bootstrap-select/bootstrap-select';
import CollapseItem from '../../../collapse-item/collapse-item';

export default class RegionFilter extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            cities: [],
        };

        this.handleCityChange = this.handleCityChange.bind(this);
    }

    componentDidMount() {
        API.get(`catalog/regions/cities`)
        .then(response => {
            this.setState({ 
                cities: response.data,
            });
        })
        .catch(error => console.log(error));
    }

    handleCityChange(name, city) {
        this.props.onChange(name, city);
    }

    render() {
        return (
            <CollapseItem target="region-filter" title="Регион" open>
                <Row>
                    <Col xs={12}>
                        <BootstrapSelect
                            name="regions"
                            title="Город" 
                            onChange={this.handleCityChange}
                            disabled={this.props.isLoading}
                            selectTextFormat="count > 2"
                            multiple
                            liveSearch
                        >
                            <optgroup label="Популярные">
                            {
                                this.state.cities.filter(city => city.isTop).map(city => (
                                    <option key={city.id} value={city.id}>{city.name}</option>
                                ))
                            }
                            </optgroup>
                            <optgroup label="Все">
                            {
                                this.state.cities.map(city =>(
                                    <option key={city.id} value={city.id}>{city.name}</option>
                                ))
                            }
                            </optgroup>
                        </BootstrapSelect>
                    </Col>
                </Row>
            </CollapseItem>
        );
    }
}