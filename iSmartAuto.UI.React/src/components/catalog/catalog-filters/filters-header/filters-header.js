import React from 'react';

function FiltersHeader(props) {
    return (
        <React.Fragment>
            <h2 className="filters-header">Найдите лучший <b>автомобиль.</b></h2>
            <div className="filters-subline">
                <span>550 000+</span><span> б/у автомобилей в продаже по всей России.</span>
            </div>
            <span className="filters-choice">Какой автомобиль вы хотите?</span>
        </React.Fragment>
    );
}

export default FiltersHeader;