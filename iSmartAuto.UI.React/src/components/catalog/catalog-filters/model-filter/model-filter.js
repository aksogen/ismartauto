import React from 'react';
import API from "./../../../../utils/API";
import { Col, Row } from 'react-bootstrap';
import BootstrapSelect from '../../../controls/bootstrap-select/bootstrap-select';
import CollapseItem from '../../../collapse-item/collapse-item';

export default class ModelFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            marks: [],
            models: [],
            generations: [],
            isMarkSelected: false,
            isModelSelected: false,
            selectedMarkId: null,
            selectedModelId: null
        };

        this.handleMarkChange = this.handleMarkChange.bind(this);
        this.handleModelChange = this.handleModelChange.bind(this);
        this.handleGenerationChange = this.handleGenerationChange.bind(this);
    }

    componentDidMount() {
        API.get(`catalog/cars/marks`)
        .then(response => {
            this.setState({ 
                marks: response.data,
            });
        })
        .catch(error => console.log(error));
    }


    handleMarkChange(type, markId) {
        API.get(`catalog/cars/${markId}/models`)
        .then(response => {
            this.setState({
                isMarkSelected: true,
                isModelSelected: false,
                selectedMarkId: markId,
                models: response.data,
                generations: [],
            });
        })
        .catch(error => console.log(error));

        let filter = [
            {
                mark: markId,
            }
        ];

        this.props.onChange(filter);
    }

    handleModelChange(type, modelId) {
        API.get(`catalog/cars/${this.state.selectedMarkId}/${modelId}`)
        .then(response => {
            this.setState({
                isModelSelected: true,
                selectedModelId: modelId,
                generations: response.data,
            });
        })
        .catch(error => console.log(error));

        let filter = [
            {
                mark: this.state.selectedMarkId,
                model: modelId,
            }
        ];

        this.props.onChange(filter);
    }

    handleGenerationChange(type, genIds) {
        let filter = [];

        if (genIds.length === 0) {
            filter = [
                {
                    mark: this.state.selectedMarkId,
                    model: this.state.selectedModelId,
                }
            ];
        }
        else {
            genIds.forEach(id => {
                filter.push({
                    mark: this.state.selectedMarkId,
                    model: this.state.selectedModelId,
                    generation: id,
                })
            })
        }

        this.props.onChange(filter);
    }


    render() {
        return (
            <CollapseItem target="model-filter" title="Модель" open>
                <Row>
                    <Col xs={6}>
                        <BootstrapSelect
                            name="mark"
                            title="Марка"
                            onChange={this.handleMarkChange}
                            disabled={this.props.isLoading}
                            liveSearch
                        >
                            <optgroup label="Популярные">
                            {
                                this.state.marks.filter(mark => mark.isTop).map(mark => (
                                    <option key={mark.id} value={mark.id}>{mark.name}</option>
                                ))
                            }
                            </optgroup>
                            <optgroup label="Все">
                            {
                                this.state.marks.map(mark =>(
                                    <option key={mark.id} value={mark.id}>{mark.name}</option>
                                ))
                            }
                            </optgroup>
                        </BootstrapSelect>
                    </Col>
                    <Col xs={6}>
                        <BootstrapSelect 
                            name="model"
                            title="Модель" 
                            onChange={this.handleModelChange}
                            disabled={!this.state.isMarkSelected || this.props.isLoading}
                        >
                        {
                            this.state.models.map(models =>(
                                <option key={models.id} value={models.id}>{models.name}</option>
                            ))
                        }
                        </BootstrapSelect>
                    </Col>
                    <Col xs={12} className="mt-3">
                        <BootstrapSelect 
                            name="generation"
                            title="Поколение"
                            onChange={this.handleGenerationChange} 
                            disabled={!this.state.isModelSelected || this.props.isLoading}
                            multiple
                        >
                        {
                            this.state.generations.map(generations =>(
                                <option key={generations.id} value={generations.id}>{generations.name}</option>
                            ))
                        }
                        </BootstrapSelect>
                    </Col>
                </Row>
            </CollapseItem>
        );
    }
}