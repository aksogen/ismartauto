import React from 'react';
import ToggleSwitch from '../../../controls/toggle-switch/toggle-switch';

export default class StateFilter extends React.Component {
    constructor(props) {
        super(props);

        this.handleUsedChange = this.handleUsedChange.bind(this);
    }

    handleUsedChange(name, value) {
        this.props.onChange(name, value);
    }

    render() {
        return (
            <React.Fragment>
                <ToggleSwitch
                    name="isOnlyUsed"
                    onChange={this.handleUsedChange}
                    label='Только б\у авто'
                    disabled={this.props.isLoading}
                />
                <ToggleSwitch 
                    name="isOnlyUnbroken"
                    onChange={this.handleUsedChange}
                    label='Кроме битых'
                    disabled={this.props.isLoading}
                    checked
                />
            </React.Fragment>
        );
    }
}