import React from 'react';
import { Col, Row } from 'react-bootstrap';
import BootstrapSelect from '../../../controls/bootstrap-select/bootstrap-select';
import CollapseItem from '../../../collapse-item/collapse-item';

export default class PriceFilter extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            priceOptions: [],
        };

        this.handlePriceFromChange = this.handlePriceFromChange.bind(this);
        this.handlePriceToChange = this.handlePriceToChange.bind(this);
    }

    componentDidMount() {
        var numbers = [];
        var n = 50000;
        for (let i = 0; i <= 50000000; i = i + n ) {
            if(i === 1000000){
                n = 100000;
            }
            else if (i === 2000000){
                n = 200000;
            }
            else if (i === 3000000){
                n = 500000;
            }
            else if (i === 5000000){
                n = 1000000;
            }
            numbers.push(i);
        }

        var opts = numbers.map((number) =>
            <option key={number} value={number} >
                {number}
            </option>
        )

        this.setState({ 
            priceOptions: opts
        });
    }

    handlePriceFromChange(name, priceFrom) {
        this.props.onChange(name, priceFrom);
    }

    handlePriceToChange(name, priceTo) {
        this.props.onChange(name, priceTo);
    }

    render() {

        return (
            <CollapseItem target="price-filter" title="Бюджет">
                <Row>
                    <Col xs={6}>
                        <BootstrapSelect 
                            name="min" 
                            title="От" 
                            onChange={this.handlePriceFromChange}
                            disabled={this.props.isLoading}
                            >
                            {this.state.priceOptions}
                        </BootstrapSelect>
                    </Col>
                    <Col xs={6}>
                        <BootstrapSelect 
                            name="max" 
                            title="До" 
                            onChange={this.handlePriceToChange}
                            disabled={this.props.isLoading}
                            >
                            {this.state.priceOptions}
                        </BootstrapSelect>
                    </Col>
                </Row>
            </CollapseItem>
        );
    }
}