import React from 'react';
import { Row, Col } from 'react-bootstrap';
import PriceTag from './price-tag/price-tag';
import NumberFormat from 'react-number-format';
import './catalog-item.scss'

export default class CatalogItem extends React.Component {
    render() {
      return (
        <div className="catalog-item">
            <img className="catalog-item-img" src={this.props.data.imageUrl} alt="Car" />
            {/* eslint-disable-next-line */}
            <a href={this.props.data.url} className="stretched-link" target="_blank"></a>
            <div className="catalog-item-body">
                <div className="item-title">{this.props.data.carInfo.fullModel}, {this.props.data.documentInfo.year}</div>
                <Row className="car-specs d-flex justify-content-between">
                    <Col>
                        <div className="specs-item d-flex align-items-center">
                            <div className="specs-item-icon">
                                <svg className="icon"><use href="#icon-engine"></use></svg>
                            </div>
                            <div className="specs-item-text">{this.props.data.techInfo.engineCapacity} л / {this.props.data.techInfo.horsePower} л.с.</div>
                        </div>
                        <div className="specs-item d-flex align-items-center">
                            <div className="specs-item-icon">
                                <svg className="icon"><use href="#icon-fuel-rate"></use></svg>
                            </div>
                            <div className="specs-item-text">{this.props.data.techInfo.fuelRate} л / 100 км</div>
                        </div>
                        <div className="specs-item d-flex align-items-center">
                            <div className="specs-item-icon">
                                <svg className="icon">
                                    <use href="#icon-speedometr"></use>
                                </svg>
                            </div>
                            <NumberFormat 
                                className="specs-item-text"
                                value={this.props.data.stateInfo.mileage}
                                displayType={'text'} 
                                thousandSeparator={' '}
                                suffix=" км"
                            />
                        </div>
                    </Col>
                    <Col>
                        <div className="specs-item d-flex align-items-center">
                            <div className="specs-item-icon">
                                <svg className="icon">
                                    <use href="#icon-engine-type"></use>
                                </svg>
                            </div>
                            <div className="specs-item-text">{this.props.data.techInfo.engineType === "GASOLINE" ? "Бензин" : "Дизель"}</div>
                        </div>
                        <div className="specs-item d-flex align-items-center">
                            <div className="specs-item-icon">
                                <svg className="icon">
                                    <use href="#icon-transmission"></use>
                                </svg>
                            </div>
                            <div className="specs-item-text">{this.props.data.techInfo.transmissionType === "AUTOMATIC" ? "АКПП" : "МКПП"}</div>
                        </div>
                        <div className="specs-item d-flex align-items-center">
                            <div className="specs-item-icon">
                                <svg className="icon">
                                    <use href="#icon-chassis"></use>
                                </svg>
                            </div>
                            <div className="specs-item-text">
                                {
                                    this.props.data.techInfo.gearType === "FORWARD_CONTROL" ? "FWD" 
                                    : this.props.data.techInfo.gearType === "REAR_DRIVE" ? "RWD"
                                    : "4WD"
                                }
                            </div>
                        </div>
                    </Col>
                </Row>
                <div className="price-info d-flex justify-content-between align-items-start">
                    <div className="price-value d-flex align-items-center">
                        <NumberFormat 
                            value={this.props.data.priceInfo.actualPrice} 
                            displayType={'text'} 
                            thousandSeparator={' '} 
                        />
                        <div className="price-value-icon">
                            <svg className="icon">
                                <use href="#icon-ruble"></use>
                            </svg>
                        </div>
                    </div>
                    <PriceTag 
                        tagType={this.props.data.priceInfo.priceTagType}
                        predictedPrice={this.props.data.priceInfo.predictedPrice}
                    />
                </div>
                <div className="region-info mt-3 d-flex align-items-center">
                    <div className="region-icon">
                        <svg className="icon">
                            <use href="#icon-marker"></use>
                        </svg>
                    </div>
                    <div className="region-text">{this.props.data.sellerInfo.region}</div>
                </div>
            </div>
        </div>
      );
    }
}


