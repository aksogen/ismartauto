import React from 'react'
import PriceTagPopover from './../price-tag-popover/price-tag-popover';
import NumberFormat from 'react-number-format';
import './price-tag.scss'

export default class PriceTag extends React.Component {
    render() {
        const { tagType } = this.props;

        switch(tagType) {
            case 1:
                return (  
                    <PriceTagPopover 
                        title="Хрошая цена"
                        type={"good"}
                        popoverContent={this.getGoodExcellentPopoverContent()}
                    >
                        <div className="price-tag-good">Хрошая цена</div>
                    </PriceTagPopover>
                );
            case 2:
                return (
                    <PriceTagPopover 
                        title="Отличная цена" 
                        type={"excellent"}
                        popoverContent={this.getGoodExcellentPopoverContent()}
                    >
                        <div className="price-tag-excellent">Отличная цена</div>
                    </PriceTagPopover>
                );
            case 0:
                return (
                    <PriceTagPopover 
                        title="Без оценки" 
                        type={"no"}
                        popoverContent={this.getNoPopoverContent()}
                    >
                        <div className="price-tag-no">Без оценки</div>
                    </PriceTagPopover>
                );
            default:
                return null;
        }
    }

    getGoodExcellentPopoverContent() {
        return(
            <React.Fragment>
                <span className="d-inline-block">Рыночная цена этого авто:</span>
                <div className="price-value mb-3 d-flex align-items-center">
                    &asymp;
                    <NumberFormat 
                        value={this.props.predictedPrice} 
                        displayType={'text'} 
                        thousandSeparator={' '} 
                    />
                    <div className="price-value-icon">
                        <svg className="icon">
                            <use href="#icon-ruble"></use>
                        </svg>
                    </div>
                </div>
                <p>Мы рассчитали рыночную цену с учетом: поколения, пробега, года, мощности, 
                    количества владельцев, расхода топлива, типа кузова, двигателя, трансмиссии.</p>
                <p>Не учитываем новые, битые, дороже 3млн, старше 1990 года.</p>
                <p>Оценка носит лишь вспомогательный характер. Реальное состояние необходимо проверять "вживую".</p>
            </React.Fragment>
        );
    }

    getNoPopoverContent() {
        return(
            <React.Fragment>
                <p>Не даем оценку новым автомобилям, битым, дороже 3 миллионов рублей и старше 1990 года.</p>
                <p>Оценка носит лишь вспомогательный характер. Реальное состояние необходимо проверять "вживую".</p>
            </React.Fragment>
        );
    }
}