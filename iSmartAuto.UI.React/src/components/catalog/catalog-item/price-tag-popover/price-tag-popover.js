import React from 'react'
// eslint-disable-next-line
import { OverlayTrigger, Tooltip, Overlay, Popover } from 'react-bootstrap';
import './price-tag-popover.scss'

export default class PriceTagPopover extends React.Component {
    popover = (
        <Popover className="price-tag-popover">
          <Popover.Title as="h3" className={"popover-header-" + this.props.type}>{this.props.title}</Popover.Title>
          <Popover.Content>
            {this.props.popoverContent}
          </Popover.Content>
        </Popover>
    );

    render() {
        return(
            <OverlayTrigger 
                trigger={['hover', 'focus']} 
                placement="top"
                delay={{ show: 400, hide: 0 }} 
                overlay={this.popover}
                rootClose
            >
                {this.props.children}
            </OverlayTrigger>
        );
    }
}