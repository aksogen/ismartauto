import React from 'react';
import { Row } from 'react-bootstrap';
import CatalogItem from './../catalog-item/catalog-item'
import './catalog-item-list-loader.scss'

export default class CatalogItemList extends React.Component {
    render() {
        return (
            <div className="catalog-item-list">
                {
                    this.props.isLoading &&
                    <div className="catalog-item-list-loader-underlay">
                        <div className="catalog-item-list-loader d-none d-lg-block">
                            <svg className="car"><use href="#car-loader"></use></svg>
                        </div>
                    </div>
                }
                <Row className="catalog-item-list-container no-gutters">
                    {
                        this.props.items.map(item =>(
                            <div key={item.id} className="col-12 col-sm-6 col-xxl-4 col-xxxl-3">
                                <div className="catalog-item-container animated fadeIn faster">
                                    <CatalogItem key={item.id} data={item}/>
                                </div>
                            </div>
                        ))
                    }
                </Row>
            </div>
        );
    }
}