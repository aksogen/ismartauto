import React from 'react'
import $ from 'jquery';
import API from "./../../utils/API";
import { Col } from 'react-bootstrap';
import './catalog-main.scss'
import './catalog-filters.scss'
import CatalogSort from './catalog-sort/catalog-sort';
import CatalogItemList from './catalog-item-list/catalog-item-list';
import CatalogPagination from './catalog-pagination/catalog-pagination';
import FiltersHeader from './catalog-filters/filters-header/filters-header'
import StateFilter from './catalog-filters/state-filter/state-filter';
import ModelFilter from './catalog-filters/model-filter/model-filter';
import PriceFilter from './catalog-filters/price-filter/price-filter';
import RegionFilter from './catalog-filters/region-filter/region-filter';


export default class Catalog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            filter: {
                isOnlyUsed: false,
                isOnlyUnbroken: true,
            },
            sort: null,
            items: [],
            page: 1,
            pageSize: 20,
            totalItemCount: 0,
            totalPageCount: null,
        };

        this.handlePageChange = this.handlePageChange.bind(this);
        this.handleSortChange = this.handleSortChange.bind(this);
        this.handleStateFilterChange = this.handleStateFilterChange.bind(this);
        this.handleModelFilterChange = this.handleModelFilterChange.bind(this);
        this.handlePriceFilterChange = this.handlePriceFilterChange.bind(this);
        this.handleRegionFilterChange = this.handleRegionFilterChange.bind(this);
    }   

    componentDidMount() {
        this.receivePageData();
    }


    receivePageData() {
        //window.history.pushState("state", "title", `http://localhost:3000/cars?page=${this.state.page}`);
        let url = `cars/search?page=${this.state.page}&pageSize=${this.state.pageSize}`;

        if (this.state.sort != null) {
            url = url + `&sort=${this.state.sort}`;
        }

        API.post(url, JSON.stringify(this.state.filter))
        .then(response => {
            console.log(response);
            this.setState({ 
                isLoading: false,
                items: response.data.items,
                totalItemCount: response.data.totalItemCount,
                totalPageCount: response.data.totalPageCount,
            });
        })
        .catch(error => {
            console.log(error);
            this.setState({ 
                isLoading: false,
            });
            this.props.history.push('/500');
        });
    }

    handlePageChange(pageNumber) {
        $('html, body').animate({scrollTop : 0}, 300);
        this.setState({
            isLoading: true,
            page: pageNumber
        }, () => {
            this.receivePageData()
        });
    }

    handleSortChange(sortType) {
        this.setState({
            sort: sortType,
            page: 1,
            isLoading: true,
        }, () => {
            this.receivePageData()
        });
    }

    handleStateFilterChange(type, value) {
        let newFilter = this.state.filter;
        newFilter[type] = value;

        this.setState({
            filter: newFilter,
            page: 1,
            isLoading: true,
        }, () => {
            this.receivePageData()
        });
    }

    handleModelFilterChange(value) {
        let newFilter = this.state.filter;
        newFilter["catalogCarFilter"] = value;

        this.setState({
            filter: newFilter,
            page: 1,
            isLoading: true,
        }, () => {
            this.receivePageData()
        });
    }

    handlePriceFilterChange(type, value) {
        let newFilter = this.state.filter;
        let priceRange = newFilter.priceRange;

        if(priceRange === undefined) {
            priceRange = {};
        }

        priceRange[type] = value;
        newFilter['priceRange'] = priceRange;

        this.setState({
            filter: newFilter,
            page: 1,
            isLoading: true,
        }, () => {
            this.receivePageData()
        });
    }

    handleRegionFilterChange(type, value) {
        let newFilter = this.state.filter;
        newFilter[type] = value;

        this.setState({
            filter: newFilter,
            page: 1,
            isLoading: true,
        }, () => {
            this.receivePageData()
        });
    }

    render() {
        return (
            <React.Fragment>
                <Col xs={12} lg={4} xl={3}>
                    <div className="catalog-filters">
                        <FiltersHeader />
                        <div className="filter-collapses">
                            <div className="filter-section border-0">
                                <StateFilter
                                    onChange={this.handleStateFilterChange}
                                    isLoading={this.state.isLoading} 
                                />
                            </div>
                            <div className="filter-section">
                                <RegionFilter 
                                    onChange={this.handleRegionFilterChange}
                                    isLoading={this.state.isLoading} 
                                />
                            </div>
                            <div className="filter-section">
                                <ModelFilter 
                                    onChange={this.handleModelFilterChange}
                                    isLoading={this.state.isLoading} 
                                />
                            </div>
                            <div className="filter-section">
                                <PriceFilter 
                                    onChange={this.handlePriceFilterChange}
                                    isLoading={this.state.isLoading}
                                />
                            </div>
                        </div>
                    </div>
                </Col>
                <Col xs={12} lg={8} xl={9}>
                    <div className="catalog-main">    
                        <CatalogSort 
                            onChange={this.handleSortChange}
                            isLoading={this.state.isLoading}
                        />
                        <CatalogItemList 
                            items={this.state.items}
                            isLoading={this.state.isLoading}
                        />
                        <CatalogPagination 
                            page={this.state.page}
                            pageSize={this.state.pageSize}
                            totalItemCount={this.state.totalItemCount}
                            onChange={this.handlePageChange}
                            isLoading={this.state.isLoading}
                        />
                    </div>
                </Col>
            </React.Fragment>
        );
    }
}