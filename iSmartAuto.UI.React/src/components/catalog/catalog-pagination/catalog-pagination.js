import React from 'react';
import Pagination from "react-js-pagination";
import './catalog-pagination.scss'

export default class CatalogPagination extends React.Component {
    render() {
      if(this.props.isLoading) {
        return null;
      }
      return (
        <div className="catalog-pagination">
            <Pagination
                activePage={this.props.page}
                itemsCountPerPage={this.props.pageSize}
                totalItemsCount={this.props.totalItemCount}
                onChange={this.props.onChange}
                pageRangeDisplayed={10}
                itemClass="page-item"
                linkClass="page-link"
                prevPageText='Предыдущая'
                nextPageText='Следующая'
                innerClass='pagination justify-content-center flex-wrap'
                hideFirstLastPages
            />
        </div>
      );
    }
}

