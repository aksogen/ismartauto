import React from 'react';

export default class SortItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            isASC: true,
        };
        this.handleClick = this.handleClick.bind(this);
  	}
    
    handleClick() {
        let sortDirection = this.state.isASC ? "asc" : "desc";
        let sortType = `${this.props.sortBy}-${sortDirection}`;
        this.props.onChange(this.props.id, sortType);

        this.setState({
            isASC: (this.props.singleDirection) ? true : !this.state.isASC,
        });
    }

    render() {
        return (
            <button 
                className={"sort-item d-flex align-items-center" + (this.props.isActive ? ' active' : '')}
                onClick={this.handleClick}
            >{this.props.text}
                <span className="sort-item-icon">
                    <svg className={"icon" + (this.props.singleDirection ? ' d-none' : '')}>
                        <use href={this.state.isASC ? '#icon-desc' : '#icon-asc'}></use>
                    </svg>
                </span>
            </button>
        );
    }
}
