import React from 'react';
import SortItem from './sort-item/sort-item';
import './catalog-sort.scss'

export default class CatalogSort extends React.Component {
    constructor(props) {
		super(props);

		this.state = {
			sortItems: [ 
				{ id: 1, text: "Цена", sortBy: "price", isSingleDirection: false }, 
				{ id: 2, text: "Пробег", sortBy: "mileage", isSingleDirection: false}, 
				{ id: 3, text: "Год", sortBy: "year", isSingleDirection: false}, 
				{ id: 4, text: "Актуальность", sortBy: "actuality", isSingleDirection: true} 
			],
			activeSortItemId: 4,
		};

		this.handleSortItemChange = this.handleSortItemChange.bind(this);
  	}
    
    handleSortItemChange(itemId, value) {
		this.setState({ activeSortItemId: itemId });
		this.props.onChange(value);
    }
	
    render() {
	  const { sortItems, activeSortItemId } = this.state;

      return (
        <div className="catalog-sort d-none d-lg-flex align-items-center">
            <span className="sort-title">Сортировать:</span>

			{
				sortItems.map(sortItem => (
					<SortItem 
						key={sortItem.id}
						id={sortItem.id}
						text={sortItem.text}
						sortBy={sortItem.sortBy}
						singleDirection={sortItem.isSingleDirection}
						onChange={this.handleSortItemChange}
						isActive={activeSortItemId === sortItem.id}
					/>
				))
			}
        </div>
      );
    }
}
