import React from 'react';
import './header.scss'

export default class Header extends React.Component {
   
    render() {
        return (
            <header className="header">
                <div className="header-top">
                    <a className={"header-logo" + (this.props.isCenterLogo ? ' center' : '')} href="/">
                        <img src={require('./../../assets/img/header-logo.png')} className="icon icon-header-logo" alt="Header logo">
                        </img>
                    </a>
                </div>
                <div className="header-tray"></div>
            </header>
        );
    }
}