import React from 'react';
import Header from '../header/header';
import './error.scss';

export default function Error(props) {
  return (
    <React.Fragment>
      <Header isCenterLogo />
      <div id="error" className="d-flex justify-content-center align-items-center">
        <div className="error-container">
          <h1 className="error-code">{props.errorCode}</h1>
          <h2 className="error-reason">{props.errorReason}</h2>
          <p className="error-text">
            {props.errorText}
            <a className="error-home-link" href="/">Вернуться на главную</a>
          </p>
        </div>
      </div>
    </React.Fragment>
  );
}