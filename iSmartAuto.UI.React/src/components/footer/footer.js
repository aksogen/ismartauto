import React from 'react';
import { Row, Col } from 'react-bootstrap';
import './footer.scss'

export default class Footer extends React.Component {
   
    render() {
        return (
            <footer className="footer">
                <div className="footer-wrapper">
                    <div className="footer-container">
                        <Row className="footer-general-links">
                            <Col xs={12} md={6} lg={3} className="footer-links">
                                <div className="footer-links-title">O Smart Auto</div>
                                <p className="footer-about">Smart Auto - это учебный проект, который демонстрирует возможности
                                анализа данных с применением машинного обучения для оценки стоимости поддержанного
                                автомобиля, тем самым предоставляя больше информации покупателям и помогая им с выбором.
                                </p>
                            </Col>
                            <Col xs={12} md={6} lg={3} className="footer-links">
                                <div className="footer-links-title">Полезные ссылки</div>
                                <a className="footer-link d-flex" href="/">Новости</a>
                                <a className="footer-link d-flex" href="/">Контакты</a>
                                <a className="footer-link d-flex" href="/">Как это работает</a>
                            </Col>
                            <Col xs={12} md={6} lg={3} className="footer-links">
                                <div className="footer-links-title">Поддержка</div>
                                <a className="footer-link d-flex" href="/">Карта сайта</a>
                                <a className="footer-link d-flex" href="/">Политика обработки данных</a>
                                <a className="footer-link d-flex" href="/">Как присоединиться к агрегатору</a>
                            </Col>
                            <Col xs={12} md={6} lg={3} className="footer-links">
                                <div className="footer-links-title">Smart Auto в социальных сетях</div>
                                <a className="footer-link d-flex align-items-center" href="/">
                                    <svg className="icon icon-social-facebook">
                                        <use href="#icon-social-facebook"></use>
                                    </svg>
                                    <span>Facebook</span>
                                </a>
                                <a className="footer-link d-flex align-items-center" href="/">
                                    <svg className="icon icon-social-facebook">
                                        <use href="#icon-social-vkontakte"></use>
                                    </svg>
                                    <span>ВКонтакте</span>
                                </a>
                                <a className="footer-link d-flex align-items-center" href="/">
                                    <svg className="icon icon-social-facebook">
                                        <use href="#icon-social-twitter"></use>
                                    </svg>
                                    <span>Twitter</span>
                                </a>
                                <a className="footer-link d-flex align-items-center" href="/">
                                    <svg className="icon icon-social-facebook">
                                        <use href="#icon-social-instagram"></use>
                                    </svg>
                                    <div>Instagram</div>
                                </a>
                            </Col>
                        </Row>
                        <Row></Row>
                    </div>
                    <div className="footer-rights">
                        <p>Настоящий сайт носит исключительно ознакомительных характер и является учебным проектом, а также
                        не является коммерческой разработкой.
                        </p>
                        <p>Представленные на изображениях автомобили могут отличаться от реальных. 
                        Помните о том, что их выставляет владелец.
                        </p>
                        <p>Любая информация, содержащаяся на настоящем сайте, носит исключительно справочный 
                        характер и ни при каких обстоятельствах не может быть расценена как предложение 
                        заключить договор (публичная оферта). 
                        Smart Auto не дает гарантий по поводу своевременности, точности и полноты информации 
                        на веб-сайте, а также по поводу беспрепятственного доступа к нему в любое время. 
                        Технические характеристики и оборудование автомобилей, цены и комплектации автомобилей, указанные на сайте, также указываются 
                        продавцом и могут быть изменены в любое время без предварительного уведомления.
                        </p>
                    </div>
                </div>
            </footer>
        );
    }
}