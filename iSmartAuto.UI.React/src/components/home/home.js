import React from 'react';
import { Container, Row } from 'react-bootstrap';
import Catalog from './../catalog/catalog'
import Header from './../header/header';
import Footer from './../footer/footer';

export default class Home  extends React.Component {

  render() {
    return (
      <React.Fragment>
        <Header />
        <main>
          <div className="main-wrapper">
            <Container fluid>
              <Row>
                <Catalog history={this.props.history}/>
              </Row>
            </Container>
          </div>  
        </main>
        <Footer />
      </React.Fragment>
    );
  }
}
